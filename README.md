
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About the Library system

Library system storages books information and has the following specifications:

* A book has: name, author, category (select with autocomplete), published date, user (person that borrowed a book).
* A category has: name, description, manybooks.
* You have to be able to do the following things with the books:
  * Show a paginated list/table with all the books.
  * Filter the list/table mentioned above.
  * Create a book.
  * Edit a book.
  * Delete a Book.
  * Be able to know if a user borrowed a book or if it still available.
  * Be able to change the status from available to not available (use a modal for this feature).
  
 
##Prerequisites
You will need to make sure your server meets the following requirements:
 * PHP >= 7.2.5
 * Mysql >= 8.0
 * Apache >= 2.4
 * Composer installed.


## Installation


1.- Download or clone the project code from Gitlab.com, into your local server folder (ex. www, htdocs).

 * Clone with SSH
 
  git@gitlab.com:ariana.franco/library.git

 * Clone with HTTPS
 
  https://gitlab.com/ariana.franco/library.git

2.-  Create a mysql database to be used later, remember the name as it will be defined later on.
   
    
3.- Configure the file .env you can copy the .env.example (make sure you remove the ".example"), you need to configure the following information:
 ```
  DB_CONNECTION=mysql
  DB_HOST=host_IP(127.0.0.1)
  DB_PORT=3306
  DB_DATABASE=database_name
  DB_USERNAME=username
  DB_PASSWORD=password
```
4.- Open the command line and go to the folder you just created with the clone of the project.

   * type "composer install".
   * type "php artisan migrate", it will create the database structure including the tables.
   * type "php artisan db:seed", it will fill the tables with testing information.
   * type "php artisan key:generate", it will generate a key that is required to start the project.
   * type "php artisan serve", it will start the project.

5.- You will see something like this:
```
Laravel development server started: http://127.0.0.1:8000
[Fri Aug 28 17:35:55 2020] PHP 7.4.0 Development Server (http://127.0.0.1:8000) started
[Fri Aug 28 17:35:57 2020] 127.0.0.1:49163 Accepted
```

Your project will be enable on the http://127.0.0.1:8000 url. 

*If you prefer yo can create a virtual host to run the project.