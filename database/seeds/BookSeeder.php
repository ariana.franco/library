<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('books')->truncate();
    
        Book::create(['name' => 'The last samurai', 'author' => 'Hellen DeWitt', 'published_date' => '2000-01-01', 'category_id' => '1']);
        Book::create(['name' => 'The corrections', 'author' => 'Jonathan', 'published_date' => '2001-01-01', 'category_id' => '2']);
        Book::create(['name' => 'Never let me go', 'author' => 'Kazuo Ishiguro', 'published_date' => '2005-01-01', 'category_id' => '3']);
        Book::create(['name' => 'How should a person be?', 'author' => 'Sheila Heti', 'published_date' => '2010-01-01', 'category_id' => '4']);
        Book::create(['name' => 'The neapolitan novels', 'author' => 'Elena Ferrante', 'published_date' => '2011-01-01', 'category_id' => '5']);
        Book::create(['name' => 'The argonauts', 'author' => 'Maggie Nelson', 'published_date' => '2015-01-01', 'category_id' => '6']);
        Book::create(['name' => '2666', 'author' => 'Roberto Bolaño', 'published_date' => '2008-01-01', 'category_id' => '7']);
        Book::create(['name' => 'The sellout', 'author' => 'Paul beatty', 'published_date' => '2015-01-01', 'category_id' => '8']);
        Book::create(['name' => 'The outline trilogy', 'author' => 'Rachel cusk', 'published_date' => '2014-01-01', 'category_id' => '9']);
        Book::create(['name' => 'Atonement', 'author' => 'Ian McEwan', 'published_date' => '2001-01-01', 'category_id' => '1']);
        Book::create(['name' => 'The Year of Magical Thinking', 'author' => 'Joan Didion', 'published_date' => '2005-01-01', 'category_id' => '2']);
        Book::create(['name' => 'Leaving the Atocha Station', 'author' => 'Ben Lerner', 'published_date' => '2011-01-01', 'category_id' => '3']);
        Book::create(['name' => 'The Flamethrowers', 'author' => 'Rachel Kushner', 'published_date' => '2013-01-01', 'category_id' => '4']);
        Book::create(['name' => 'Erasure', 'author' => 'Percival Everett', 'published_date' => '2001-01-01 ', 'category_id' => '5']);
        Book::create(['name' => 'Middlesex', 'author' => 'Jeffrey Eugenides', 'published_date' => '2002-01-01', 'category_id' => '6']);
        Book::create(['name' => 'Platform', 'author' => 'Michel Houellebecq', 'published_date' => '2002-01-01', 'category_id' => '7']);
        Book::create(['name' => 'Do Everything in the Dark', 'author' => 'Dary Indiana', 'published_date' => '2003-01-01', 'category_id' => '1']);
        Book::create(['name' => 'The Known World', 'author' => 'Edward P. Jones', 'published_date' => '2003-01-01', 'category_id' => '2']);
        Book::create(['name' => 'The Plot Against America', 'author' => 'Philip Roth', 'published_date' => '2004-01-01', 'category_id' => '2']);
        Book::create(['name' => 'The Line of Beauty', 'author' => 'Alan Hollinghusrt', 'published_date' => '2004-01-01', 'category_id' => '3']);
        Book::create(['name' => 'Veronica', 'author' => 'Mary Gaitskill', 'published_date' => '2005-01-01', 'category_id' => '6']);
        Book::create(['name' => 'The Road', 'author' => 'Cormac McCarthy', 'published_date' => '2006-01-01', 'category_id' => '9']);
        Book::create(['name' => 'Ooga-Booga', 'author' => 'Frederick Seidel', 'published_date' => '2006-01-01', 'category_id' => '8']);
    }
}
