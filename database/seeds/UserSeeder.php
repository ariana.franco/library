<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('users')->truncate();
        
        User::create(['name' => 'Ariana', 'last_name' => 'Franco']);
        User::create(['name' => 'Juan', 'last_name' => 'Ramirez']);
        User::create(['name' => 'Diego', 'last_name' => 'Hernández']);
    }
}
