<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('categories')->truncate();
    
    
        Category::create(['name' => 'Action', 'description' => 'Action books is a book genre in which the protagonist or protagonists are thrust into a series of events that typically include violence, extended fighting, physical feats and frantic chases.']);
        Category::create(['name' => 'Comedy', 'description' => 'Comedy is a literary genre and a type of dramatic work that is amusing and satirical in its tone, mostly having a cheerful ending.']);
        Category::create(['name' => 'Drama', 'description' => 'drama is a category of narrative fiction (or semi-fiction) intended to be more serious than humorous in tone..']);
        Category::create(['name' => 'Fantasy', 'description' => 'Another description of a Fantasy Novel is any book that contains unrealistic settings, or magic, often set in a medieval universe, or possibly involving mythical beings or supernatural forms as a primary element of the plot, theme, or setting.']);
        Category::create(['name' => 'Horror', 'description' => 'Horror is a genre of speculative fiction which is intended to frighten, scare, or disgust.']);
        Category::create(['name' => 'Mystery', 'description' => 'The mystery genre is a type of fiction in which a detective, or other professional, solves a crime or series of crimes.']);
        Category::create(['name' => 'Romance', 'description' => 'a romance is a narrative genre in literature that involves a mysterious, adventurous, or spiritual story line where the focus is on a quest that involves bravery and strong values, not always a love interest.']);
        Category::create(['name' => 'Thriller', 'description' => 'Thriller is a genre of fiction, having numerous, often overlapping subgenres. Thrillers are characterized and defined by the moods they elicit, giving viewers heightened feelings of suspense, excitement, surprise, anticipation and anxiety.']);
        Category::create(['name' => 'Western', 'description' => 'Western is a genre of fiction set primarily in the latter half of the 19th century in the Western United States, which is styled the "Old West".']);
    }
}
