$(document).ready(function() {

    /**
     * Adding CRSF token to all Ajax request
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if(typeof message !== 'undefined') {
        swal.fire({
            position         : 'center',
            icon             : 'success',
            title            : message,
            showConfirmButton: false,
            timer            : 1500
        });
    }

    $('.page-item a:eq(1)').addClass('active');


    /**
     * navigating through the padding pages.
     */
    $('#navigationBooks').on('click', '.page-item a', function() {
        $('.page-item a').removeClass('active');

        $(this).addClass('active');

        var page = $(this).attr('data-value');

        var filter = $('#bookSearch').val();
        searchData(page, filter);

    });


    /**
     * Functionality to search books with the filter option in the table data.
     */
    $('#bookSearch').keyup(function(event) {
        event.preventDefault();
        load_data();

    });

    /**
     * Loading table data from server.
     */
    function load_data() {
        var page = $('.page-item a.active').attr('data-value');

        var filter = $('#bookSearch').val();

        searchData(page > 0 ? page : 1, filter);
    }

    /**
     * Searching data on the server.
     * @param page
     * @param filter
     */
    function searchData(page, filter = '') {

        $.ajax({
            url    : '/list_books',
            type   : 'POST',
            async  : true,
            data   : {
                p: page,
                f: filter
            },
            success: function(response) {
                var dataBooks = $.parseJSON(response);

                if(dataBooks['data'].length > 0) {
                    $('.items-table-book').html('');

                    var html = '';
                    dataBooks['data'].forEach(function(book) {
                        var availabilityText = book.availability == true ? 'Available' : 'Unavailable';
                        html += `
                        <tr>
                            <td>${book.id}</td>
                            <td>${book.name}</td>
                            <td>${book.author}</td>
                            <td>${book.category.name}</td>
                            <td>${book.published_date_format}</td>
                            <td>${availabilityText}</td>
                            <td>`
                                if(book.availability == true ){
                                    html += `
                                    <button type="button" class="btn btn-info btn_lend_book" data-toggle="modal" data-target="#modalLendBook"
                                        data-value="${book.id}" data-book="${book.name}">
                                        Lend book
                                        </button>`;
                                        }else{
                                    html += `<button type="button" class="btn btn-info btn_return_book" data-toggle="modal" data-target="#modalReturnBook"
                                            data-value="${book.id}" data-book="${book.name}" data-user="${book.borrowed_user.id}" data-borrowed-to="${book.borrowed_user.name + ' ' + book.borrowed_user.last_name}">
                                        Return book
                                    </button>`;
                                }
                        html += `</td>
                            <td><button type="button" class="btn btn-primary btn_edit" data-value="${book.id}">Edit</button></td>
                            <td><button type="button" class="btn btn-danger btn_delete" data-value="${book.id}" >Delete</button></td>
                        </tr>
                    `;
                    });

                    var htmlPaginator = `
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous" data-value="1">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>`;

                    for(i = 1; i <= dataBooks['numPages']; i++) {
                        htmlPaginator += `
                        <li class="page-item">
                            <a class="page-link" href="#" data-value="${i}">${i}</a>
                        </li>`;
                    }

                    htmlPaginator += `
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next" data-value="${dataBooks['numPages']}">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    `;

                    var currentPage = $('.page-item a.active').attr('data-value');
                    if(currentPage >= dataBooks['numPages']) {
                        currentPage = dataBooks['numPages'];
                    }

                    var pageMaxNum  = page * 10 < dataBooks['numItems'] ? page * 10 : dataBooks['numItems'];
                    var pageMinNum  = page * 10 - 9;
                    var htmlEntries = `Showing ${pageMinNum} to ${pageMaxNum} of ${dataBooks['numItems']} entries`;
                }
                else {
                    html = `
                        <tr>
                            <td colspan="6">
                                <div class="alert alert-danger">
                                    No data found.
                                </div>
                            </td>
                        </tr>`;

                    htmlPaginator = '';
                    htmlEntries   = '';
                }

                $('.items-table-book').html(html);
                $('ul.pagination').html(htmlPaginator);

                $('#infoEntries').html(htmlEntries);

                $('.page-item a:eq(' + currentPage + ')').addClass('active');


            },
            error  : function(error) {
                console.log(error);

            }
        });
    }

    /**
     * Redirect to create a book.
     */
    $('#createBook').click(function(event) {
        event.preventDefault();
        location.href = '/book';
    });


    $('#btnRegisterBook').click(function() {


        if($('#category_name').val() != '' && ($('#category_id').val() == null || $('#category_id').val() == '' )){
            swal.fire(
                'Validation!',
                'You have to choose a category from the list.',
                'error'
            );

            return false;
        }

    });

    /**
     * Redirect to edit a book.
     */
    $('.btn_edit').on('click', function(e) {
        var idBook = $(this).attr('data-value');

        location.href = '/book/edit/'+idBook;

    });

    /**
     * Deleting a book
     */
    $('.items-table-book').on('click', '.btn_delete', function() {
        var idBook = $(this).attr('data-value');

        //Confirming info delete.
        swal.fire({
            title             : 'Are you sure you want to delete the book with id ' + idBook + '?',
            text              : 'You won\'t be able to revert this!',
            icon              : 'warning',
            showCancelButton  : true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor : '#d33',
            confirmButtonText : 'Yes, delete it!'
        }).then((result) => {
            if(result.value) {

                $.ajax({
                    url    : '/book/delete',
                    type   : 'DELETE',
                    async  : true,
                    data   : {
                        id: idBook
                    },
                    success: function(response) {
                        if(response == 'ok') {
                            swal.fire(
                                'Deleted!',
                                'The book has been deleted.',
                                'success'
                            );

                            load_data();

                        }
                        else {
                            swal.fire(
                                'Error!',
                                response,
                                'error'
                            );
                        }


                    },
                    error  : function(error) {
                        console.log(error);
                    }
                });

            }
        });
    });

    /**
     * Button cancel
     */
    $('#btnCancel').on('click', function(e) {
        location.href = '/';
    });


    /**
     * Functionality to create the autocomplete input for categories
     */

    $('#category_name').keyup(function() {
        $('#category_id').val(null);

        var filter = $(this).val();
        if(filter != '') {

            $.ajax({
                url    : '/categories',
                method : 'POST',
                data   : {
                    f: filter
                },
                success: function(response) {

                    var dataCategories = $.parseJSON(response);


                    var html = '<ul class="list-group list-group-flush categories">';

                    dataCategories.forEach(function(category) {
                        html += `
                        <li data-value="${category.id}" data-name="${category.name}" class="list-group-item d-flex justify-content-between align-items-center"><p>${category.name}</p>: <b>${category.description}</b><span class="badge badge-primary badge-pill">${category.many_books} Books</span></li>
                    `;
                    });

                    html += '</ul>';


                    $('#categoriesList').fadeIn();
                    $('#categoriesList').html(html);
                }
            });
        }
    });

    $(document).on('click', '.categories li', function() {
        $('#category_name').val($(this).attr('data-name'));
        $('#category_id').val($(this).attr('data-value'));
        $('#categoriesList').fadeOut();
    });


    /**
     * Functionality to create the autocomplete input for users
     */

    $('#user_name').keyup(function() {
        $('#id_user').val(null);

        var filter = $(this).val();
        if(filter != '') {

            $.ajax({
                url    : '/users',
                method : 'POST',
                data   : {
                    f: filter
                },
                success: function(response) {

                    var dataUsers = $.parseJSON(response);


                    var html = '<ul class="list-group list-group-flush users">';

                    dataUsers.forEach(function(user) {
                        html += `
                        <li data-value="${user.id}" data-name="${user.name} ${user.last_name}" class="list-group-item d-flex justify-content-between align-items-center">${user.name} ${user.last_name}</li>
                    `;
                    });

                    html += '</ul>';


                    $('#usersList').fadeIn();
                    $('#usersList').html(html);
                }
            });
        }
    });

    $(document).on('click', '.users li', function() {
        $('#user_name').val($(this).attr('data-name'));
        $('#id_user').val($(this).attr('data-value'));
        $('#usersList').fadeOut();
    });


    /**
     * Giving value to category when it's editable
     */
    if(typeof category !== 'undefined') {
        $('#category_name').val(category);
    }


    /**
     *Returning a book to the library.
     */
    $(document).on('click', '.btn_return_book', function() {
        var idBook = $(this).attr('data-value');
        var idUser = $(this).attr('data-user');
        var borrowedTo = $(this).attr('data-borrowed-to');
        var nameBook = $(this).attr('data-book');
        $('#id_book').val(idBook);
        $('#id_user').val(idUser);

        $('#modalReturnBook p.details').html('<p class="details">Libro: '+ nameBook +'</br>Borrowed to: ' + borrowedTo +'</p>');
    });

    /**
     *Returning a book to the library.
     */
    $(document).on('click', '.save-return-book', function() {

        var delivered_date = $('#real_delivered_date').val();

        if(delivered_date == ''){
            return null;

        }else{

            $.ajax({
                url    : '/book/return',
                type   : 'POST',
                async  : true,
                data   : {
                    id_book:  $('#id_book').val(),
                    id_user:  $('#id_user').val(),
                    delivered_date:  $('#real_delivered_date').val(),
                    observations:  $('#observations').val()
                },
                success: function(response) {
                    if(response == 'ok') {
                        $('#modalReturnBook').modal('toggle');


                        swal.fire(
                            'Returned book!',
                            'The book has been checked as available.',
                            'success'
                        );


                        load_data();

                    }
                    else {
                        swal.fire(
                            'Error!',
                            response,
                            'error'
                        );
                    }


                },
                error  : function(error) {
                    console.log(error);
                }
            });
        }

    });


    /**
     * Lending a book to an user.
     */
    $(document).on('click', '.btn_lend_book', function() {
        modalTurnOnUser(false);
        $('#estimated_delivered_date').val(null);
        $('#id_user').val(null);
        $('#user_name').val(null);

        var idBook = $(this).attr('data-value');
        var nameBook = $(this).attr('data-book');
        $('#id_book').val(idBook);

        $('#modalLendBook p.details').html('<p class="details">Libro: '+ nameBook );
    });

    /**
     *Lending a book to an user.
     */
    $(document).on('click', '.save-lend-book', function() {

        var delivered_date = $('#estimated_delivered_date').val();
        var user_name = $('#user_name').val();
        var name = $('#name').val();
        var last_name = $('#last_name').val();

        //validating delivered date and user_name when exists
        if(delivered_date == '' || ($('#user_name').attr('required') && user_name == '' )){
            return null;

        }

        //Validating user selection on autocomplete input.
        if($('#user_name').attr('required') && ($('#id_user').val() == null || $('#id_user').val() == '' )){

            swal.fire(
                'Validation!',
                'You have to choose a user from the list.',
                'error'
            );

            return false;
        }

        //Validating form new user.
        if($('#name').attr('required') && (name == '' || last_name == '')){

            return null;

        }else{

            $('#user_name').removeAttr('required');

            $.ajax({
                url    : '/book/lend',
                type   : 'POST',
                async  : true,
                data   : {
                    id_book:  $('#id_book').val(),
                    id_user:  $('#id_user').val(),
                    delivered_date:  $('#estimated_delivered_date').val(),
                    user_name:  name,
                    last_name:  last_name
                },
                success: function(response) {
                    if(response == 'ok') {
                        $('#name').val('');
                        $('#last_name').val('');
                        $('#modalLendBook').modal('toggle');


                        swal.fire(
                            'Lended book!',
                            'The book has been checked as unavailable.',
                            'success'
                        );


                        load_data();

                    }
                    else {
                        swal.fire(
                            'Error!',
                            response,
                            'error'
                        );
                    }


                },
                error  : function(error) {
                    console.log(error);
                }
            });
        }

    });

    $(document).on('click', '#createUser', function() {
        modalTurnOnUser(true);
    });

    $(document).on('click', '#searchUser', function() {
        modalTurnOnUser(false);
    });

    function modalTurnOnUser(showCreateUser){
        $('#name').val('');
        $('#last_name').val('');

        if(showCreateUser == true){
            $('#formCreateUser').show();
            $('#searchUser').show();
            $('#usernameListTag').hide();
            $('#createUser').hide();
            $('#user_name').removeAttr('required');
            $('#name').attr('required', 'required');
            $('#last_name').attr('required', 'required');

        }else{
            $('#formCreateUser').hide();
            $('#searchUser').hide();
            $('#usernameListTag').show();
            $('#createUser').show();
            $('#user_name').attr('required', 'required');
            $('#name').removeAttr('required');
            $('#last_name').removeAttr('required');
        }
    }

});
