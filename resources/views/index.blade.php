<!DOCTYPE html>
<div class="page-wrapper bg-bkgd p-t-10 p-b-10 font-robo">
    <!-- Page Header -->
@include('layouts.header')

<!-- Books list -->
@include('books.list')

<!-- Page Footer -->
    @include('layouts.footer')

</div>



@if (session('message'))
    <script>
        var message = '{{ session('message') }}';
    </script>
@endif
