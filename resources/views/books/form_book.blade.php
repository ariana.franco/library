<div class="col-lg-8 col-md-10 mx-auto">
    <div class="card text-center">
        <div class="card-header col-sm-12">
            <p>Register new book</p>
        </div>
        <div class="card-body col-sm-12">
            <div class="row">
                <div class="offset-md-1 col-sm-10">

                    <div class="row">
                        <div class="col-sm-4">{!!Form::label('name', 'Book name: ')!!}</div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12 floating-label-form-group controls">
                                {!! Form::text("name", null ,["id"=>"name", "class"=>"form-control","required"=>"required", "placeholder"=>"Book name", "autofocus", "autocomplete" =>"off"]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">{!!Form::label('author', 'Author name: ')!!}</div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12 floating-label-form-group controls">
                                {!!Form::text("author", null ,["id"=>"author", "class"=>"form-control","required"=>"required", "placeholder"=>"Author name", "autofocus", "autocomplete" =>"off"]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">{!!Form::label('published_date', 'Published date: ')!!}</div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12 floating-label-form-group controls">
                                {!!Form::date("published_date", null ,["id"=>"published_date", "class"=>"datepicker form-control","required"=>"required", "placeholder"=>"Published date", "autofocus", "autocomplete" =>"off"]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">{!!Form::label('category_name', 'Category: ')!!}</div>
                        <div class="col-sm-6">
                            <div class="form-group col-sm-12 floating-label-form-group controls">
                                {!!Form::text("category_name", null ,["id"=>"category_name", "class"=>"form-control autocomplete","required"=>"required", "placeholder"=>"Category", "autofocus", "autocomplete" =>"off"]) !!}
                                {!!Form::hidden("category_id", null ,["id"=>"category_id", "class"=>"form-control"]) !!}

                                <div id="categoriesList" class="overflow-auto" style="max-height: 200px;"></div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            {!! Form::button('<i class="fa fa-plus"></i> Cancel', array('id' => 'btnCancel', 'class' => 'btn btn-danger btn-block', 'type' => 'button')) !!}

                        </div>
                        <div class="offset-2 col-sm-4">
                            {!! Form::submit('Register', array('id' => 'btnRegisterBook', 'class' => 'btn btn-primary btn-block', 'type' => 'submit')) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


