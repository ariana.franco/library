<div class="modal fade" id="modalReturnBook" tabindex="3" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Return book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p class="details"></p>
                {!!Form::open([
                    'id'       => 'frm-return-book',
                    'method'   => 'POST',
                    'onSubmit' => 'return false;'
                ])!!}
                {!!Form::hidden("id_book", null ,["id"=>"id_book"]) !!}
                {!!Form::hidden("id_user", null ,["id"=>"id_user"]) !!}

                <div class="form-group">
                    {!!Form::label('real_delivered_date', 'Real delivered date: ')!!}
                    {!!Form::date("real_delivered_date", null ,["id"=>"real_delivered_date", "class"=>"datepicker form-control","required"=>"required", "placeholder"=>"Delivered date", "autofocus", "autocomplete" =>"off"]) !!}

                </div>

                <div class="form-group">
                    {!!Form::label('observations', 'Observations: ')!!}
                    <textarea id="observations" class="form-control" id="message-text"></textarea>
                </div>

                <hr>
                <div class="form-group center" style="text-align: center">
                    {!! Form::button('Close', array('class' => 'btn btn-secondary', 'data-dismiss' => 'modal', 'type' => 'button')) !!}
                    {!! Form::submit('Register', array('class' => 'btn btn-primary save-return-book', 'type' => 'button')) !!}
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
