<div class="modal fade" id="modalLendBook" tabindex="3" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Lend book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <p class="details"></p>
                {!!Form::open([
                    'id'       => 'frm-lend-book',
                    'method'   => 'POST',
                    'onSubmit' => 'return false;'
                ])!!}
                {!!Form::hidden("id_book", null ,["id"=>"id_book"]) !!}


                <div class="form-group">
                    {!!Form::label('estimated_delivered_date', 'Delivered date: ')!!}
                    {!!Form::date("estimated_delivered_date", null ,["id"=>"estimated_delivered_date", "class"=>"datepicker form-control","required"=>"required", "placeholder"=>"Delivered date", "autofocus", "autocomplete" =>"off"]) !!}

                </div>


                <div class="form-group" id="usernameListTag">
                    {!!Form::label('user_name', 'Select an user: ')!!}
                    {!! Form::text("user_name", null ,["id"=>"user_name", "class"=>"form-control autocomplete","required"=>"required", "placeholder"=>"User", "autofocus", "autocomplete" =>"off"]) !!}
                    {!! Form::hidden("id_user", null ,["id"=>"id_user", "class"=>"form-control"]) !!}

                    <div id="usersList" class="overflow-auto" style="max-height: 100px;"></div>

                </div>


                <a href="#" id="createUser">Create new user</a>
                <a href="#" id="searchUser">Search user</a>
                <div class="hidden" id="formCreateUser">
                    <hr>
                    <p class="font-italic" style="font-size: 15px;">
                            **Please fill this form only if you want to create a new user.**
                    </p>
                    <div class="form-group">
                        {!!Form::label('name', 'User name: ')!!}
                        {!!Form::text("name", null ,["id"=>"name", "class"=>"form-control", "placeholder"=>"User name", "autofocus", "autocomplete" =>"off"]) !!}
                    </div>
                    <div class="form-group">
                        {!!Form::label('last_name', 'User lastname: ')!!}
                        {!!Form::text("last_name", null ,["id"=>"last_name", "class"=>"form-control", "placeholder"=>"User lastname", "autofocus", "autocomplete" =>"off"]) !!}
                    </div>
                    <hr>
                </div>
                <div class="form-group center" style="text-align: center">
                    {!! Form::button('Close', array('class' => 'btn btn-secondary', 'data-dismiss' => 'modal', 'type' => 'button')) !!}
                    {!! Form::submit('Register', array('class' => 'btn btn-primary save-lend-book', 'type' => 'button')) !!}
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
