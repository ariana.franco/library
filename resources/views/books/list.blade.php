<div class="col-lg-10 col-md-10 mx-auto">
    <div class="card text-center">
        <div class="card-header col-sm-12">
            <h2>Books</h2>
        </div>

        <div class="card-body col-sm-12">

            <div class="row">
                <div class="offset-md-1 col-md-2">
                    {!! Form::button('<i class="fa fa-plus"></i> Add book', array('id' => 'createBook', 'class' => 'btn btn-success btn-lg btn-block ', 'type' => 'button')) !!}

                </div>
                <div class="col-md-8">
                    <div class="input-group mb-3">
                        {!!Form::text('book', null, ['id' => 'bookSearch', 'class' => 'required form-control', 'placeholder' => 'Search Book'])!!}
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="5%">ID</th>
                        <th width="38%">Name</th>
                        <th width="57%">Author</th>
                        <th width="57%">Category</th>
                        <th width="57%">Published date</th>
                        <th>Availability</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="items-table-book">
                    @foreach($data as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->author }}</td>
                            <td>{{ $row->category->name }}</td>
                            <td>{{ $row->published_date_format }}</td>
                            <td>{{ $row->availability == true ? 'Available' : 'Unavailable'}}</td>
                            <td>
                                @if($row->availability == true )
                                <button type="button" class="btn btn-info btn_lend_book" data-toggle="modal" data-target="#modalLendBook"
                                        data-value="{{ $row->id }}" data-book="{{ $row->name }}">
                                    Lend book
                                </button>
                                @else
                                    <button type="button" class="btn btn-info btn_return_book" data-toggle="modal" data-target="#modalReturnBook"
                                            data-value="{{ $row->id }}" data-book="{{ $row->name }}" data-user="{{ $row->borrowed_user->id }}" data-borrowed-to="{{ $row->borrowed_user->name.' '.$row->borrowed_user->last_name}}">
                                        Return book
                                    </button>
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary btn_edit"
                                        data-value="{{ $row->id }}">
                                    Edit
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger btn_delete"
                                        data-value="{{ $row->id }}">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @include('paginator')
            </div>
        </div>
    </div>
</div>
@include('books.modal_return_book')
@include('books.modal_lend_book')
