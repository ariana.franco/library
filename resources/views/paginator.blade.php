<nav aria-label="Page navigation" id="navigationBooks">
    <p id="infoEntries">Showing 1 to 10 of {{$num_books}} entries</p>

    <ul class="pagination">
        <li class="page-item">
            <a class="page-link disabled" href="#" aria-label="Previous" data-value="1">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        @for($i = 1; $i<=$num_pages; $i++)
            <li class="page-item">
                <a class="page-link" href="#" data-value="{{$i}}">{{$i}}</a>
            </li>
        @endfor
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next" data-value="{{$num_pages}}">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>

    </ul>
</nav>