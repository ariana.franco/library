<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Library system</title>
    <meta name="description" content="SIID">
    <meta name="author" content="Ariana Franco">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" media="all" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" media="all" href="/css/styles-app.css">
    <link rel="stylesheet" media="all" href="/css/styles.css">

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/js/books.js"></script>

    <div id="headerWrapper">
        <!-- Page Header -->
        <div id="header">
            <a href="/" id="logo" class="full"></a>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">

                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Books</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/book">Add new book</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <header class="masthead" style="background-image: url('/images/header-img.jpg')">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-10 mx-auto">
                        <div class="page-heading">
                            <h1>My Library</h1>
                            <span class="subheading"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
</head>