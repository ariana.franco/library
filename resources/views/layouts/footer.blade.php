<footer>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <p class="copyright text-muted">Copyright &copy; Ariana Franco 2020</p>
                </div>
            </div>
        </div>
    </footer>
</footer>

@section('scripts')
    <!--Scripts-->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-autocomplete.min.js"></script>
@stop