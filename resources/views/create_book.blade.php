<!DOCTYPE html>
<div class="page-wrapper bg-bkgd p-t-10 p-b-10 font-robo">
    <!-- Page Header -->
    @include('layouts.header')

    <!-- Book form -->

    {!!Form::open([
        'id'       => 'frm-book',
        'method'   => 'POST',
        'action'   => 'BookController@store'
    ])!!}
    @include('books.form_book')
    {!!Form::close()!!}

    <!-- Page Footer -->
    @include('layouts.footer')

</div>



@if (session('message'))
    <script>
        var message = '{{ session('message') }}';
    </script>
@endif
