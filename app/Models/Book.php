<?php
    
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Book extends Model {
    protected $table    = 'books';
    protected $fillable = [
        'name',
        'author',
        'published_date',
        'category_id'
    ];
    
    public $appends = ['published_date_format', 'availability', 'borrowed_user'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function borrowedBooks() {
        return $this->belongsToMany('App\Models\User', 'borrowed_books', 'book_id', 'user_id')
                    ->withPivot('estimated_delivered_date', 'real_delivered_date', 'observations')->withTimestamps();
        
    }

    /**
     * @return string
     */
    public function getPublishedDateFormatAttribute() {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['published_date'])->format('d/m/Y');
    }

    /**
     * @return bool
     */
    public function getAvailabilityAttribute() {
        $borrowed = $this->borrowedBooks->last();
        
        if($borrowed && is_null($borrowed->pivot->real_delivered_date)) {
            return false;
        }
        
        return true;
    }

    /**
     * @return null|string
     */
    public function getBorrowedUserAttribute() {
        $borrowed = $this->borrowedBooks->last();
   
        if($borrowed && $borrowed->real_delivered_date == null) {
            return $borrowed;
        }
    
        return null;
    }
    
    
}
