<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    protected $table    = 'categories';
    protected $fillable = [
        'name',
        'description'
    ];
    
    public $appends  = ['many_books'];
    
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books() {
        return $this->hasMany('App\Models\Book', 'category_id', 'id');
    }
    
    /**
     * @return int
     */
    public function getManyBooksAttribute() {
        
        return $this->books()->count();
    }
    
}
