<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class User extends Model{
    
    protected $table    = 'users';
    protected $fillable = [
        'name',
        'last_name'
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function borrowedBooks() {
        return $this->belongsToMany('App\Models\Book', 'borrowed_books', 'user_id', 'book_id')
                    ->withPivot('estimated_delivered_date', 'real_delivered_date', 'observations')->withTimestamps();
        
    }
    
}
