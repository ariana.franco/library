<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function index(Request $request) {
        
        $filter = $request->has('f') ? $request->get('f') : null;
        
        //Looking users by name.
        
        if(!is_null($filter))
            $user = User::where('name', 'like', '%' . $filter . '%')
                              ->orWhere('last_name', 'like', '%' . $filter . '%')->orderBy('name', 'asc')->get();
        else
            $user = User::all();
        
        return json_encode($user);
    }
}
