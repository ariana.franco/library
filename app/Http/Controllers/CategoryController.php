<?php
    
namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller {
    
    public function index(Request $request) {
        
        $filter = $request->has('f') ? $request->get('f') : null;
        
        //Looking categories by name.
        
        if(!is_null($filter))
            $categories = Category::where('name', 'like', '%' . $filter . '%')->orderBy('name', 'asc')->get();
        else
            $categories = Category::all();
        
        return json_encode($categories);
    }
}
