<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;

class BookController extends Controller {
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('create_book');
    }
    
    
    /**
     * @param Request $request
     * @return false|string
     */
    public function list(Request $request) {
        $page       = $request->has('p') ? $request->get('p') : 1;
        $numResults = $request->has('n') ? $request->get('n') : 10;
        $filter     = $request->has('f') ? $request->get('f') : null;
        
        $limit = $numResults;
        $skip  = ($page - 1) * $limit;
        
        $books = null;
        
        if(!is_null($filter))
            $books = Book::where('name', 'like', '%' . $filter . '%')
                         ->orWhere('author', 'like', '%' . $filter . '%');
        
        if($books == null)
            $books = Book::orderBy('id', 'asc');
        
        $numItems = $books->count();
        
        $booksArray = $books->orderBy('id', 'asc')
                            ->limit($limit)
                            ->skip($skip)
                            ->get();
        
        foreach($booksArray as $book) {
            $book->category;
        }
        
        $pages    = $numItems / 10;
        $numPages = intval($pages) < $pages ? intval($pages) + 1 : intval($pages);
        
        return json_encode(['data' => $booksArray, 'numPages' => $numPages, 'numItems' => $numItems]);
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->validate([
            'name'           => 'required',
            'author'         => 'required',
            'published_date' => 'required',
            'category_id'    => 'required'
        ]);
    
        $book = Book::create($request->all());
        
        return redirect('/')->with('message', 'Book '.$book->name.' created successfully');
    }
    
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id) {
        $book = Book::find($id);
        
        return view('edit_book', compact(['book']));
    }
    
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {
        $book = Book::find($id);
        
        return view('edit_book', compact(['book']));
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request) {
        $request->validate([
            'name'           => 'required',
            'author'         => 'required',
            'published_date' => 'required',
            'category_id'    => 'required'
        ]);
        
        $id = $request->get('book_id');
        $params = $request->only(['name','author','published_date','category_id']);
        
        Book::where('id', $id)->update($params);
    
        return redirect('/')->with('message', 'Book '.$params['name'].' updated successfully');
        
    }
    
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request) {
        $book = Book::find($request->get('id'));
        
        if(!$book->availability){
            return "The book cannot be deleted due to it is borrowed.";
        }
        
        if($book->delete()) {
            return 'ok';
        }
        else {
            return 'An error has ocurred';
        }
    }
    
    /**
     * @param Request $request
     * @return string
     */
    public function returnBook(Request $request){
        $params = $request->all();
        
        $book = Book::find($params['id_book']);
        //$user = User::find($params['id_user']);
        
        
        $book->borrowedBooks->last()->pivot->update(['real_delivered_date' => $params['delivered_date'], 'observations' => $params['observations']]);
        return 'ok';
        
        
    }
    
    /**
     * @param Request $request
     * @return string
     */
    public function lendBook(Request $request){
        $params = $request->all();
        
        $book = Book::find($params['id_book']);
        
        if($params['id_user'] && !$params['user_name']){
            $user = User::find($params['id_user']);
        }else{
            $user = User::where(['name' => $params['user_name'],'last_name' => $params['last_name']])->first();
            if(!$user){
                $user = User::create(['name' => $params['user_name'],'last_name' => $params['last_name']]);
            }
            
        }
    
        $book->borrowedBooks()->attach([$user->id => ['estimated_delivered_date' => $params['delivered_date']]]);
        
        return 'ok';
        
    }


}
