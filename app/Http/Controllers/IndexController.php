<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        //Looking data for index, first page on table.
        $data = Book::orderBy('id','asc')->paginate(10);
        
        $num_books = Book::count();
        $pages = $num_books / 10;
        $num_pages =  intval($pages) < $pages ? intval($pages) + 1 : intval($pages);
    
        return view('index',compact(['data', 'num_books', 'num_pages']));
    }
    
    
}
