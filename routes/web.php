<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('index');
});*/

Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
Route::post('/list_books', ['as' => 'books', 'uses' => 'BookController@list']);


Route::delete('/book/delete','BookController@destroy');
Route::post('/book/store','BookController@store');
Route::get('/book/edit/{id}','BookController@edit')->where('id', '[0-9]+');
Route::post('/book/update','BookController@update');
Route::post('/book/return','BookController@returnBook');
Route::post('/book/lend','BookController@lendBook');
Route::get('/book','BookController@index');

Route::post('categories','CategoryController@index');
Route::post('users','UserController@index');

//Route::resource('books','BookController');
//Route::resource('users','UserController');

